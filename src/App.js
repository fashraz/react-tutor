import { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from "./components/Headers";
import ProductList from "./components/ProductLists";
import About from "./components/About";
import Contact from "./components/Contact";

function App() {

  const [title, setTitle] = useState("Welcome to my page");
  const [age, setAge] = useState(20);
  const changeTitle = () =>{
    setTitle ("Title changed");
    setAge(40);
  };

  //loopinglist
  const [products, setProducts] = useState([
    { id:1, title: 'product 1', price: 889},
    { id:2, title: 'product 2', price: 654},
    { id:3, title: 'product 3', price: 821},
    { id:4, title: 'product 4', price: 125},
    { id:5, title: 'product 5', price: 844},
  ]);

  const deleteProduct = (productId) => {
    const newProducts = products.filter(product => product.id !== productId);
    setProducts(newProducts);
  }
  
  //UseEffect dependency
  const [name,setName] = useState('Mohd');

  useEffect (() => {
    console.log('Use Effect Running');

  }, [name]);

  return (
    <div>
      <Header />
      <h1>{title}</h1>
      <h1>Age: {age +1}</h1>
      <button onClick={ changeTitle }>change titlenya</button>
      
      <br></br>

      <button onClick={() => setName('Fash')}>Change Name</button>
      <p>{name}</p>

      <br></br>

    <Router>
      <Switch>
        <Route  path="/">
            <ProductList products={products} deleteProduct={deleteProduct}/>
        </Route>
        <Route  path="/about">
            <About/>
        </Route>       
        <Route  path="/contact">
            <Contact/>
        </Route>
      </Switch>
    </Router>
    </div>
  );
}

export default App;
